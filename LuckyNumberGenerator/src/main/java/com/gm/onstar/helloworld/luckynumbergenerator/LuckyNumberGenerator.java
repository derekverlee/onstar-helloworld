package com.gm.onstar.helloworld.luckynumbergenerator;

public class LuckyNumberGenerator {
    private int seed;

    public static LuckyNumberGenerator getGenerator(int seed) {
        return new LuckyNumberGenerator(seed);

    }


    LuckyNumberGenerator(int seed) {
        this.seed = seed;
    }

    public long[] getLuckyNumbers(int[] digitGroups) {
        long[] luckyNums = new long[digitGroups.length];
        for(int i = 0; i < luckyNums.length; i++) {
            luckyNums[i] = 12*i;
        }
        return luckyNums;
    }

}
