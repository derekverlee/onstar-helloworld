package com.gm.onstar.helloworld.luckynumbergenerator;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class LuckyNumberGeneratorTest {

    static final int[] EMPTY_INT_ARRAY = {};


    public static class IsEmptyArrayOfLongs extends BaseMatcher<long[]> {

        @Override public boolean matches(Object o) {
            return (o!=null) && (o instanceof long[]) && (((long[]) o).length==0);
        }

        @Override public void describeMismatch(Object o, Description description) {
            description.appendText(" was ").appendValue(o);
        }


        @Override public void describeTo(Description description) {
            description.appendText("expected an empty array of long");
        }
    }

    public static IsEmptyArrayOfLongs isEmptyArrayOfLongs() {
        return new IsEmptyArrayOfLongs();
    }

    @Test
    public void testGetLuckyNumbers() throws Exception {
        LuckyNumberGenerator subject = LuckyNumberGenerator.getGenerator(0);

        assertNotNull(subject);


        assertThat(subject.getLuckyNumbers(EMPTY_INT_ARRAY), isEmptyArrayOfLongs());

    }
}
