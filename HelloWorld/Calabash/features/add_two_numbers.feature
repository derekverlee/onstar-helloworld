Feature: Addition of 2 integers

  @HelloWorld-Test-HappyPath-1
  Scenario: The hello world app is capable of simple addition of two integers
    Given I enter "2" into input field number 1
    And I enter "5" into input field number 2
    When I press "Add" 
    Then I should see text containing "7"

  @HelloWorld-Test-HappyPath-2
  Scenario: The hello world app is capable of simple addition of two integers
    Given I enter "5" into input field number 1
    And I enter "3" into input field number 2
    When I press "Add" 
    Then I should see text containing "8"


  @HelloWorld-Test-EditText-1-input-more-than-10
    Scenario: The hello world app is capable to validate the input integers
    Given I enter "40" into input field number 1
    And I enter "5" into input field number 2
    When I press "Add" 
    Then I should see text containing "Please enter numbers between 1 and 10"

  @HelloWorld-Test-EditText-2-input-more-than-10
    Scenario: The hello world app is capable to validate the input integers
    Given I enter "5" into input field number 1
    And I enter "50" into input field number 2
    When I press "Add" 
    Then I should see text containing "Please enter numbers between 1 and 10"

  @HelloWorld-Test-EditText-1-&-2-input-more-than-10
    Scenario: The hello world app is capable to validate the input integers
    Given I enter "50" into input field number 1
    And I enter "50" into input field number 2
    When I press "Add" 
    Then I should see text containing "Please enter numbers between 1 and 10"
