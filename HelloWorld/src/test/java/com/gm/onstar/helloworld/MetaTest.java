package com.gm.onstar.helloworld;

import android.app.Activity;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(RobolectricTestRunner.class)
public class MetaTest {



	@Test
	public void this_test_should_pass() {
		assert true;
	}
//
//	@Test
//	public void this_test_should_fail() {
//		fail();
//	}

	@Test
	public void should_reference_something_from_project() {
		Activity a = Robolectric.buildActivity(HelloWorldActivity.class).create().get();
		assertNotNull(a);
		TextView label = (TextView) a.findViewById(R.id.textViewId);
		assertThat(label.getText().toString(), equalTo("Hello, world!"));
	}



}
