package com.gm.onstar.helloworld;

import android.app.AlertDialog;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gm.onstar.helloworld.utilities.InputValidationResult;
import com.gm.onstar.helloworld.utilities.OperandsInputValidator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.tester.android.view.TestMenu;
import org.robolectric.tester.android.view.TestMenuItem;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class HelloWorldTest {

	HelloWorldActivity activity;
	TextView resultText;
	EditText firstNumber, secondNumber;
	Button addButton, webViewButton;
	MathService mockService;


	OperandsInputValidator mockValidator;


	@Before
	public void setUp() throws Exception {

		ShadowLog.stream = System.out;
		Log.i("HelloWorld", "setup ............");
		activity = Robolectric.buildActivity(HelloWorldActivity.class).create().get();
		resultText = (TextView) activity.findViewById(R.id.textViewId);
		firstNumber = (EditText) activity.findViewById(R.id.editText);
		secondNumber = (EditText) activity.findViewById(R.id.editText2);
		addButton = (Button) activity.findViewById(R.id.button);
		webViewButton = (Button) activity.findViewById(R.id.webViewButtonId);

		mockService = mock(MathService.class);
		activity.mathService = mockService;
		when(mockService.sum(anyInt(), anyInt())).thenReturn(42);

		mockValidator = mock(OperandsInputValidator.class);
		activity.validator = mockValidator;

	}

	@Test
	public void test_addNumbers_valid() throws Exception {

		when(mockValidator.validate(anyInt(), anyInt())).thenReturn(InputValidationResult.createValid());
		firstNumber.performClick();
		firstNumber.setText("2");

		secondNumber.performClick();
		secondNumber.setText("5");

		addButton.performClick();

		assertThat(resultText.getText().toString(), equalTo("Sum of 2+5=42"));
		verify(mockService).sum(2, 5);
		verify(mockValidator).validate(2,5);
	}


	@Test
	public void test_addNumbers_invalid() throws Exception {
		final String expectedAlert = "ALERT THIS IS A TEST =)";
		when(mockValidator.validate(anyInt(), anyInt())).thenReturn(InputValidationResult.createInvalid(expectedAlert));
		firstNumber.performClick();
		firstNumber.setText("40");

		secondNumber.performClick();
		secondNumber.setText("5");

		addButton.performClick();


		AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
		ShadowAlertDialog shadowAlertDialog = Robolectric.shadowOf_(alertDialog);

		assertEquals(expectedAlert, shadowAlertDialog.getMessage() );

	}

	@Test
	public void test_click_menu_item() {


		TestMenu menu = new TestMenu(activity);
		activity.onCreateOptionsMenu(menu);
		assertNotNull("Find action must be included", menu.findItem(R.id.action_settings));

		TestMenuItem testMenu = new TestMenuItem(R.id.action_settings);
		assertNotNull(testMenu);

		assertThat("About menu item should exist", testMenu, notNullValue());
		testMenu.click();
	}

	@Test
	public void test_logMessage() {
		assert true;
	}

    /*
	@Test
    public void test_webview(){
        ShadowActivity shadowActivity = Robolectric.shadowOf_(activity);
        webViewButton.performClick();
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = Robolectric.shadowOf_(intent);
        assertEquals(shadowIntent.getComponent().getClassName(), WebViewActivity.class.getName());
    }
    */
}
