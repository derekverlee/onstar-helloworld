package com.gm.onstar.helloworld.utilities;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNull;

public class BetweenOneAndTenValidatorTest {


	OperandsInputValidator subject;

	int[] invalids = {-11,-10,-1,0,11,12,13,99,1001};

	@Before
	public void setupSubject() {
		subject = new BetweenOneAndTenValidator();
	}

	@Test
	public void testBothAreValid() {
		InputValidationResult actual;


		for(int i = 1; i < 11; i++) {
			for(int j=1; j<11; j++){

				actual = subject.validate(i,j);


				final String debugTestMsg = "Should be valid for " + i + " and " + j;
				assertTrue(debugTestMsg,actual.isValid);
				assertNull(debugTestMsg, actual.message);

			}
		}
	}

	@Test
	public void testOneInvalid() {
		InputValidationResult actual;

			for(int i :invalids) {
				for(int j=1; j<11; j++){
					actual = subject.validate(i,j);
					final String debugTestMsg = "Should be invalid for " + i + " and " + j;
					assertFalse(debugTestMsg, actual.isValid);
					assertNotNull(debugTestMsg, actual.message);
					assertEquals("Please enter numbers between 1 and 10",actual.message);
				}
			}

		for(int i=1;i<11;i++) {
			for(int j:invalids){
				actual = subject.validate(i,j);
				final String debugTestMsg = "Should be invalid for " + i + " and " + j;
				assertFalse(debugTestMsg, actual.isValid);
				assertNotNull(debugTestMsg, actual.message);
				assertEquals("Please enter numbers between 1 and 10",actual.message);
			}
		}
		}

	@Test
	public void testBothInvalid() {
		InputValidationResult actual;

		for(int i : invalids) {
			for(int j : invalids){
				actual = subject.validate(i,j);
				final String debugTestMsg = "Should be invalid for " + i + " and " + j;
				assertFalse(debugTestMsg, actual.isValid);
				assertNotNull(debugTestMsg, actual.message);
				assertEquals("Please enter numbers between 1 and 10",actual.message);
			}
		}
	}
}


