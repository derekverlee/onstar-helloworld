package com.gm.onstar.helloworld;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class MathServiceTest {


	MathService subject = new MathService();

	@Test
	public void testSumZeros() {
		assertEquals(0, subject.sum(0, 0));
		for(int n = -22; n<101; n++) {
			assertEquals(n,subject.sum(n,0));
			assertEquals(n,subject.sum(0,n));
		}
	}


	@Test
	public void testClassicSums() {
		assertEquals(2, subject.sum(1, 1));
		assertEquals(4, subject.sum(2, 2));
		assertEquals(5, subject.sum(3, 2));
		assertEquals(10, subject.sum(5, 5));
		assertEquals(-100, subject.sum(-99, -1));
		assertEquals(0, subject.sum(-1, 1));

	}
}
