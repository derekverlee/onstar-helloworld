package com.gm.onstar.helloworld;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gm.onstar.helloworld.luckynumbergenerator.LuckyNumberGenerator;
import com.gm.onstar.helloworld.utilities.BusProvider;
import com.gm.onstar.helloworld.utilities.InputValidationResult;
import com.gm.onstar.helloworld.utilities.OperandsInputValidator;
import com.squareup.otto.Bus;

import javax.inject.Inject;

public class HelloWorldActivity extends Activity {

    EditText editText, editText1;
    TextView mTextView;
    Bus eventBus;

    @Inject
    MathService            mathService;
    @Inject
    OperandsInputValidator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO: temporary -- prove dependancy on the other module
        int[] digitGroups = {3,2,3};
        long[] nums = LuckyNumberGenerator.getGenerator(123).getLuckyNumbers(digitGroups);
        Log.i("LuckyNumbers","oncreate, numbers length " + nums.length);
        for(long n : nums) Log.i("LuckyNumbers"," number is " + n);

        eventBus = BusProvider.getInstance();
        eventBus.register(this);


        Log.i("HelloWorldActivity", "onCreate ............");

        setContentView(R.layout.activity_hello_world);

        ((SampleApplication) getApplication()).inject(this);

        mTextView = (TextView) findViewById(R.id.textViewId);

        editText = (EditText) findViewById(R.id.editText);
        editText1 = (EditText) findViewById(R.id.editText2);

        Button addButton = (Button) findViewById(R.id.button);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Throwing an event onto the Bus
                eventBus.post("Clicked");
                String a = editText.getText().toString();
                String b = editText1.getText().toString();

                int a1 = Integer.parseInt(a);
                int b1 = Integer.parseInt(b);
                InputValidationResult validationResult = validator.validate(a1, b1);
                if (!validationResult.isValid) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(HelloWorldActivity.this);
                    alertDialog.setMessage(validationResult.message);
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.i("HelloWorld", "Dialog closed");
                        }
                    }).create().show();

                } else {
                    mTextView.setText("Sum of " + a + "+" + b + "=" + mathService.sum(a1, b1));
                }
            }

        });


        Button webViewButton = (Button) findViewById(R.id.webViewButtonId);
        webViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(HelloWorldActivity.this, WebViewActivity.class);
                //startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hello_world, menu);
        return true;
    }

}
