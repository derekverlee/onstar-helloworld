package com.gm.onstar.helloworld.utilities;

public interface OperandsInputValidator {
	public InputValidationResult validate(int lhs, int rhs) ;
}
