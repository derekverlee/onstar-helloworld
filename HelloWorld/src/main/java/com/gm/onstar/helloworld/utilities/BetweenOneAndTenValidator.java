package com.gm.onstar.helloworld.utilities;

public class BetweenOneAndTenValidator implements OperandsInputValidator {

	@Override
	public InputValidationResult validate(int lhs, int rhs) {
		InputValidationResult result = new InputValidationResult();

		if(lhs >= 1 && lhs <= 10 && rhs >= 1 && rhs <= 10) {
			result.isValid = true;
			return result;
		} else {
			result.isValid=false;
			result.message = "Please enter numbers between 1 and 10";
			return result;
		}

	}
}
