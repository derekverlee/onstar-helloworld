package com.gm.onstar.helloworld;

public class Log {

	public static final int ASSERT = android.util.Log.ASSERT;
	public static final int ERROR = android.util.Log.ERROR;
	public static final int WARN = android.util.Log.WARN;
	public static final int INFO = android.util.Log.INFO;
	public static final int DEBUG = android.util.Log.DEBUG;
	public static final int VERBOSE = android.util.Log.VERBOSE;

	private static int logLevel = INFO;

	public static void setLogLevel(int level) {
		String levelText = null;
		switch (level) {
		case Log.ERROR:
			levelText = "ERROR";
			break;
		case Log.WARN:
			levelText = "WARN";
			break;
		case Log.ASSERT:
			levelText = "ASSERT";
			break;
		case Log.INFO:
			levelText = "INFO";
			break;
		case Log.DEBUG:
			levelText = "DEBUG";
			break;
		case Log.VERBOSE:
			levelText = "VERBOSE";
			break;
		default:
			levelText = "VERBOSE";
			break;
		}
		i("HelloWorld", "Setting log level to " + levelText);
		logLevel = level;
	}

	public static boolean isLoggable(String tag, int level) {
		return (level >= logLevel);
		// return android.util.Log.isLoggable(tag, level);
	}

	public static int println(int priority, String tag, String msg) {
		if (!isLoggable(tag, priority))
			return 0;
		return android.util.Log.println(priority, tag, msg);
	}

	public static String getStackTraceString(Throwable tr) {
		return android.util.Log.getStackTraceString(tr);
	}

	public static int d(String tag, String msg, Throwable tr) {
		if (!isLoggable(tag, DEBUG))
			return 0;
		return android.util.Log.d(tag, msg, tr);
	}

	public static int d(String tag, String msg) {
		if (!isLoggable(tag, DEBUG))
			return 0;
		return android.util.Log.d(tag, msg);
	}

	public static int e(String tag, String msg, Throwable tr) {
		if (!isLoggable(tag, ERROR))
			return 0;
		return android.util.Log.e(tag, msg, tr);
	}

	public static int e(String tag, String msg) {
		if (!isLoggable(tag, ERROR))
			return 0;
		return android.util.Log.e(tag, msg);
	}

	public static int i(String tag, String msg, Throwable tr) {
		if (!isLoggable(tag, INFO))
			return 0;
		return android.util.Log.i(tag, msg, tr);
	}

	public static int i(String tag, String msg) {
		if (!isLoggable(tag, INFO))
			return 0;
		return android.util.Log.i(tag, msg);
	}

	public static int v(String tag, String msg, Throwable tr) {
		if (!isLoggable(tag, VERBOSE))
			return 0;
		return android.util.Log.v(tag, msg, tr);
	}

	public static int v(String tag, String msg) {
		if (!isLoggable(tag, VERBOSE))
			return 0;
		return android.util.Log.v(tag, msg);
	}

	public static int w(String tag, String msg, Throwable tr) {
		if (!isLoggable(tag, WARN))
			return 0;
		return android.util.Log.w(tag, msg, tr);
	}

	public static int w(String tag, Throwable tr) {
		if (!isLoggable(tag, WARN))
			return 0;
		return android.util.Log.w(tag, tr);
	}

	public static int w(String tag, String msg) {
		if (!isLoggable(tag, WARN))
			return 0;
		return 0;// android.util.Log.w(tag, msg);
	}

}
