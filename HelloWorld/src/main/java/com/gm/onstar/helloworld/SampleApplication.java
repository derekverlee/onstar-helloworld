package com.gm.onstar.helloworld;

import android.app.Application;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;

public class SampleApplication extends Application{


    private ObjectGraph objectGraph;

    @Override public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(getModules().toArray());
    }

    protected List<ServiceModule> getModules() {
        return Arrays.asList(
                new ServiceModule()
        );
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }
}
