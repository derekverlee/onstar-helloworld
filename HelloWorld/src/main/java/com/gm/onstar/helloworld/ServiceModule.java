package com.gm.onstar.helloworld;

import com.gm.onstar.helloworld.utilities.BetweenOneAndTenValidator;
import com.gm.onstar.helloworld.utilities.OperandsInputValidator;

import dagger.Module;
import dagger.Provides;

@Module(library = true, injects = HelloWorldActivity.class)
public class ServiceModule {

    @Provides
    MathService provideMathService() {
        return new MathService();
    }

	@Provides
	OperandsInputValidator provideValidator() {
		return new BetweenOneAndTenValidator();
	}
}