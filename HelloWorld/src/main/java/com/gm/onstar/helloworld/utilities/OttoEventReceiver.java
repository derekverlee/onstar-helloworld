package com.gm.onstar.helloworld.utilities;

import com.gm.onstar.helloworld.Log;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
public class OttoEventReceiver {

    private Bus eventBus;

    public OttoEventReceiver() {
        eventBus = BusProvider.getInstance();
        eventBus.register(this);
    }

    @Subscribe public void answerAvailable(String clicked) {
        Log.e(""+clicked,"Got an event form teh otto bus!!!");
    }

}
