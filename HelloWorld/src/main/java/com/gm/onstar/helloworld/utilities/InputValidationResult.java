package com.gm.onstar.helloworld.utilities;

public class InputValidationResult {

	public static InputValidationResult createValid() {
		InputValidationResult r = new InputValidationResult();
		r.isValid = true;
		return r;
	}

	public static InputValidationResult createInvalid(String message) {
		InputValidationResult r = new InputValidationResult();
		r.isValid = false;
		r.message = message;
		return r;
	}

	public boolean isValid;
	public String message;
}
